package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Request;

public interface RequestRepository extends CrudRepository<Request, Integer> {
}
