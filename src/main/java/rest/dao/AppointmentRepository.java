package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Appointment;

public interface AppointmentRepository extends CrudRepository<Appointment, Integer> {
}
