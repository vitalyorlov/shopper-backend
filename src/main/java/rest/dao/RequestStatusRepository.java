package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.RequestStatus;

public interface RequestStatusRepository extends CrudRepository<RequestStatus, Integer> {
}
