package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Keyword;

public interface KeywordRepository extends CrudRepository<Keyword, Integer> {
}
