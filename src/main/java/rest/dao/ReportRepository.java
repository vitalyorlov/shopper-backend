package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Report;

public interface ReportRepository extends CrudRepository<Report, Integer> {
}
