package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.StatisticItem;

public interface StatisticItemRepository extends CrudRepository<StatisticItem, Integer> {
}
