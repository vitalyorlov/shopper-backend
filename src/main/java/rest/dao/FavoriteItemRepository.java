package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.FavoriteItem;

public interface FavoriteItemRepository extends CrudRepository<FavoriteItem, Integer> {
}
