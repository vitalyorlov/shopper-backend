package rest.dao;

//import model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import rest.model.entity.User;

import java.util.List;
import java.util.stream.Stream;

public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findAll();

    List<User> findByEmail(String email);

    User findByLogin(String login);

    @Query("select c from User c where c.email = :email")
    Stream<User> findByEmailReturnStream(@Param("email") String email);

}
