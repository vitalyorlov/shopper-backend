package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.HelpCategory;

public interface HelpCategoryRepository extends CrudRepository<HelpCategory, Integer> {
}
