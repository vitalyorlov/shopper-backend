package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
