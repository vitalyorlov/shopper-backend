package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.HelpPage;

public interface HelpPageRepository extends CrudRepository<HelpPage, Integer> {
}
