package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Country;

public interface CountryRepository extends CrudRepository<Country, Integer> {
}
