package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.LikeForItem;

public interface LikeForItemRepository extends CrudRepository<LikeForItem, Integer> {
}
