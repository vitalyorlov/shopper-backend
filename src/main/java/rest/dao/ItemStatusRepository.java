package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.ItemStatus;

public interface ItemStatusRepository extends CrudRepository<ItemStatus, Integer> {
}
