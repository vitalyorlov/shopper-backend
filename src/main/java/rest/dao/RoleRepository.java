package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Role;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
