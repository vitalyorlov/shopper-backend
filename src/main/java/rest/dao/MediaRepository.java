package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Media;

public interface MediaRepository extends CrudRepository<Media, Integer> {
}
