package rest.dao.custom.repository;

import java.util.List;

/**
 * Created by d1l on 5/19/17.
 */

public interface RawQueryRepository<T> {

    List<T> executeQuery(String query);

}
