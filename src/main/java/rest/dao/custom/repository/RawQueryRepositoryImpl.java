package rest.dao.custom.repository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by d1l on 5/19/17.
 */
@Repository
public class RawQueryRepositoryImpl<T> implements RawQueryRepository<T> {

    @PersistenceContext
    private EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<T> executeQuery(String query) {
        return this.entityManager.
                createQuery(query).
                getResultList();
    }

}
