package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.City;

import java.util.List;

public interface CityRepository extends CrudRepository<City, Integer> {

    List<City> findByCountryId(Integer countryId);

}
