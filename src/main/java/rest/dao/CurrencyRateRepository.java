package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.CurrencyRate;

public interface CurrencyRateRepository extends CrudRepository<CurrencyRate, Integer> {
}
