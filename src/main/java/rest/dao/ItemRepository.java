package rest.dao;

import org.springframework.data.repository.CrudRepository;
import rest.model.entity.Item;

public interface ItemRepository extends CrudRepository<Item, Integer> {
}
