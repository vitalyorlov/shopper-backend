package rest.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import rest.model.entity.PersonalInfo;
import rest.model.entity.User;

public interface PersonalInfoRepository extends JpaRepository<PersonalInfo, Integer> {

    PersonalInfo findByUserId(Integer userId);

}
