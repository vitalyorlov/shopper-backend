package rest.model.response;

import rest.model.entity.Media;
import rest.model.entity.PersonalInfo;
import rest.model.entity.User;

/**
 * Created by d1l on 4/27/17.
 */
public class LoginResponse {

    private String accessToken;
    private String username;
    private Media userPhoto;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Media getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(Media userPhoto) {
        this.userPhoto = userPhoto;
    }

    public LoginResponse(String accessToken, PersonalInfo personalInfo) {
        this.accessToken = accessToken;
        this.setUserPhoto(personalInfo.getUser().getMedia());
        this.setUsername(personalInfo.getFirstName()
                + ' ' + personalInfo.getLastName());
    }

    public LoginResponse() {
    }

}
