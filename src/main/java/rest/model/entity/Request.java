package rest.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "REQUESTS")
public class Request {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "CREATED_DATE")
    private Timestamp createdDate;

    @ManyToOne
    @JoinColumn(name="ITEMS_ID")
    private Item item;

    @ManyToOne
    @JoinColumn(name="USERS_ID")
    private User potentialBuyer;

    @Column(name = "PRICE")
    private Float price;

    @ManyToOne
    @JoinColumn(name="CURRENCIES_ID")
    private Currency currency;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name="REQUESTS_STATUSES_ID")
    private RequestStatus status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public User getPotentialBuyer() {
        return potentialBuyer;
    }

    public void setPotentialBuyer(User potentialBuyer) {
        this.potentialBuyer = potentialBuyer;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
