package rest.model.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "MEDIA")
public class Media {

    @Id
    @GenericGenerator(name="keygen", strategy="increment")
    @GeneratedValue(generator="keygen")
    @Column(name = "ID")
    private int id;

    @Column(name = "URL")
    private String url;

    @Column(name = "FILENAME")
    private String filename;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
