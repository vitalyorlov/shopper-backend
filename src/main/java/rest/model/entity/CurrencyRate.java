package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "CURRENCIES_RATE")
public class CurrencyRate {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "RATE")
    private float rate;

    @ManyToOne(optional = false)
    @JoinColumn(name="CURRENCIES_ID")
    private Currency sourceCurrency;

    @ManyToOne(optional = false)
    @JoinColumn(name="CURRENCIES_ID1")
    private Currency targetCurrency;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public Currency getSourceCurrency() {
        return sourceCurrency;
    }

    public void setSourceCurrency(Currency sourceCurrency) {
        this.sourceCurrency = sourceCurrency;
    }

    public Currency getTargetCurrency() {
        return targetCurrency;
    }

    public void setTargetCurrency(Currency targetCurrency) {
        this.targetCurrency = targetCurrency;
    }
}
