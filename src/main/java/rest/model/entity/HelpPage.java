package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "HELP_PAGES")
public class HelpPage {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "BODY")
    private String body;

    @ManyToOne
    @JoinColumn(name="HELP_CATEGORIES_ID")
    private HelpCategory category;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public HelpCategory getCategory() {
        return category;
    }

    public void setCategory(HelpCategory category) {
        this.category = category;
    }
}
