package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "BLACKLIST_USERS")
public class BlacklistUser {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "REASON")
    private String reason;

    @ManyToOne(optional = false)
    @JoinColumn(name="USERS_ID")
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
