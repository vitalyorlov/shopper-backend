package rest.model.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "CITIES")
public class City {

    @Id
    @GenericGenerator(name="keygen", strategy="increment")
    @GeneratedValue(generator="keygen")
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @ManyToOne(optional = false)
    @JoinColumn(name="COUNTRIES_ID")
    private Country country;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
