package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "HELP_CATEGORIES")
public class HelpCategory {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name="HELP_CATEGORIES_ID")
    private HelpCategory parentCategory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HelpCategory getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(HelpCategory parentCategory) {
        this.parentCategory = parentCategory;
    }
}
