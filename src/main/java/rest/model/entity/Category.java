package rest.model.entity;

import com.sun.istack.internal.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "CATEGORIES")
public class Category {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "CATEGORIES_ID")
    @Nullable
    private Integer parentId;

    @Column(name = "NAME")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
