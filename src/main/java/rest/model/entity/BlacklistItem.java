package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "BLACKLIST_ITEMS")
public class BlacklistItem {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "REASON")
    private String reason;

    @ManyToOne(optional = false)
    @JoinColumn(name="ITEMS_ID")
    private Item item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
