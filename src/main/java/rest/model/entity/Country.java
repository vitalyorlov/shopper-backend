package rest.model.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "COUNTRIES")
public class Country {

    @Id
    @GenericGenerator(name="keygen", strategy="increment")
    @GeneratedValue(generator="keygen")
    @Column(name = "ID")
    private int id;

    @Column(name = "ABBR")
    private String abbr;

    @Column(name = "NAME")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAbbr() {
        return abbr;
    }

    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
