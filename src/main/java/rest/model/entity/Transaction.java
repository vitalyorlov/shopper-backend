package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "TRANSACTIONS")
public class Transaction {

    @Id
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name="REQUESTS_ID")
    private Request request;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
}
