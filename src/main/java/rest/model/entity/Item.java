package rest.model.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "ITEMS")
public class Item {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private Float price;

    @ManyToOne
    @JoinColumn(name="CURRENCIES_ID")
    private Currency currency;

    @ManyToOne
    @JoinColumn(name="CATEGORIES_ID")
    private Category category;

    @ManyToOne
    @JoinColumn(name="USERS_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name="ITEMS_STATUSES_ID")
    private ItemStatus itemStatus;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "ITEMS_TO_KEYWORDS", joinColumns = {
            @JoinColumn(name = "ITEMS_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "KEYWORDS_ID",
                    nullable = false, updatable = false) })
    private Set<Keyword> keywords;

    public Set<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<Keyword> keywords) {
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ItemStatus getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(ItemStatus itemStatus) {
        this.itemStatus = itemStatus;
    }
}
