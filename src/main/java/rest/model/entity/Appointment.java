package rest.model.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "APPOINTMENTS")
public class Appointment {

    @Id
    @Column(name = "ID")
    private int id;

    @Column(name = "DATE")
    private Timestamp createdDate;

    @Column(name = "ADDRESS")
    private String address;

    @ManyToOne
    @JoinColumn(name="REQUESTS_ID")
    private Request request;

    @ManyToOne
    @JoinColumn(name="CITY_ID")
    private City city;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
