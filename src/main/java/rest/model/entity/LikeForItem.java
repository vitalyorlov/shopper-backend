package rest.model.entity;

import javax.persistence.*;

/**
 * Created by d1l on 4/25/17.
 */
@Entity
@Table(name = "LIKES")
public class LikeForItem {

    @Id
    @Column(name = "ID")
    private int id;

    @ManyToOne
    @JoinColumn(name="USERS_ID")
    private User user;

    @ManyToOne
    @JoinColumn(name="ITEMS_ID")
    private Item item;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
