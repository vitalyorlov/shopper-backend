package rest.controller;

//import dao.UserRepository;
//import model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import rest.model.entity.PersonalInfo;
import rest.model.entity.User;
import rest.model.response.LoginResponse;
import rest.service.PersonalInfoService;
import rest.service.SecurityService;
import rest.service.UserService;
import rest.validator.PersonalInfoValidator;

import javax.validation.Valid;

@RestController
@CrossOrigin(origins = "*")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private PersonalInfoService personalInfoService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private PersonalInfoValidator personalInfoValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new PersonalInfoValidator(userService));
    }

    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> registration(@Valid @RequestBody PersonalInfo personalInfo) {
        String password = personalInfo.getUser().getPassword();
        personalInfo.setUser(userService.save(personalInfo.getUser()));
        personalInfoService.save(personalInfo);

        String token = securityService.login(personalInfo.getUser().getLogin(), password);

        HttpHeaders headers = new HttpHeaders();
        personalInfo.setUser(userService.findByLogin(personalInfo.getUser().getLogin()));
        LoginResponse response = new LoginResponse(token, personalInfo);

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<LoginResponse> login(@RequestBody User user) {
        System.out.println("Login " + user.getLogin());

        String token = securityService.login(user.getLogin(), user.getPassword());

        HttpHeaders headers = new HttpHeaders();
        user = userService.findByLogin(user.getLogin());
        PersonalInfo personalInfo = personalInfoService.findByUser(user);
        LoginResponse response = new LoginResponse(token, personalInfo);

        return new ResponseEntity<>(response, headers, HttpStatus.OK);
    }

}
