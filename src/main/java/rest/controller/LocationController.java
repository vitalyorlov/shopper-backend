package rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rest.model.entity.City;
import rest.model.entity.Country;
import rest.service.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public ResponseEntity<List<Country>> getCountries() {
        return new ResponseEntity<>(locationService.getCountries(),
                new HttpHeaders(),
                HttpStatus.OK);
    }

    @RequestMapping(value = "/country/{id}/cities", method = RequestMethod.GET)
    public ResponseEntity<List<City>> getCities(@PathVariable(name = "id") String countryId) {
        return new ResponseEntity<>(locationService.getCitiesByCountryId(countryId),
                new HttpHeaders(),
                HttpStatus.OK);
    }

}

