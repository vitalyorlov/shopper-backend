package rest.controller;

//import dao.UserRepository;
//import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rest.dao.UserRepository;
import rest.model.entity.Item;
import rest.model.entity.User;
import rest.service.ItemsService;
import rest.service.SecurityService;
import rest.service.UserService;
import rest.validator.PersonalInfoValidator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ItemsController {

    @Autowired
    private ItemsService itemsService;

    @Autowired
    private SecurityService securityService;

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @Transactional
    public List<Item> getItems(HttpServletRequest request, HttpServletResponse response) {
        List<Item> items = itemsService.getItems(getParameters(request));
        return items;
    }

    private Map<String, String> getParameters(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();

        for (Map.Entry<String, String[]> paramEntry : request.getParameterMap().entrySet()) {
            params.put(paramEntry.getKey(), paramEntry.getValue()[0]);
        }

        return params;
    }

}
