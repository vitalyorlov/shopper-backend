package rest.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class MediaController {

    private static final String UPLOADED_FOLDER = "/home/d1l/Documents/shopper_media/";

    @RequestMapping(value = "/media", method = RequestMethod.POST)
    public String uploadMedia(@RequestParam("file") MultipartFile file) {

        if (file.isEmpty()) {
            return "File is empty";
        }

        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "File has been uploaded";
    }

    @RequestMapping(value = "/media/{id}", method = RequestMethod.GET)
    @ResponseBody
    public HttpEntity<byte[]> getMedia(@PathVariable String id) {
        byte[] image = new byte[0];

        try {
            Path path = Paths.get(UPLOADED_FOLDER + "user.png");
            image = Files.readAllBytes(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        headers.setContentLength(image.length);

        return new HttpEntity<>(image, headers);
    }
}
