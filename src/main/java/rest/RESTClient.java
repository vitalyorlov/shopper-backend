package rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

@Component("restClient")
public class RESTClient {

    @Autowired
    private RestTemplate restTemplate;

    public String findUserByEmail() {
        return restTemplate.getForObject("http://localhost:8080/users", String.class);
    }
}
