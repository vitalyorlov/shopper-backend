package rest.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import rest.model.entity.PersonalInfo;
import rest.model.entity.User;
import rest.service.UserService;

@Component
public class PersonalInfoValidator implements Validator {

    private UserService userService;

    @Autowired
    public PersonalInfoValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return PersonalInfo.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PersonalInfo personalInfo = (PersonalInfo) o;

        //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "NotEmpty");
        if (personalInfo.getUser().getLogin().length() < 6 || personalInfo.getUser().getLogin().length() > 32) {
            errors.rejectValue("user", "Size.userForm.username");
        }
        if (userService.findByLogin(personalInfo.getUser().getLogin()) != null) {
            errors.rejectValue("user", "Duplicate.userForm.username");
        }

        // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
        if (personalInfo.getUser().getPassword().length() < 8 || personalInfo.getUser().getPassword().length() > 32) {
            errors.rejectValue("user", "Size.userForm.password");
        }

    }
}
