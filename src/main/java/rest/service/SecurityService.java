package rest.service;

public interface SecurityService {
    void authenticate(String authToken);

    String login(String username, String password);

    void logout();
}
