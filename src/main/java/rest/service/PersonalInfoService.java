package rest.service;

import rest.model.entity.PersonalInfo;
import rest.model.entity.User;

public interface PersonalInfoService {

    PersonalInfo save(PersonalInfo personalInfo);

    PersonalInfo findByUser(User user);

}
