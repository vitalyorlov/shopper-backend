package rest.service;

import rest.model.entity.Item;

import java.util.List;
import java.util.Map;

/**
 * Created by d1l on 5/19/17.
 */
public interface ItemsService {

    List<Item> getItems(Map<String, String> parameters);

}
