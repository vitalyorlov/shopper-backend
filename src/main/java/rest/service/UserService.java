package rest.service;

import rest.model.entity.User;

public interface UserService {

    User save(User user);

    User findByLogin(String login);

}
