package rest.service.impl;

import antlr.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.dao.custom.repository.RawQueryRepository;
import rest.model.entity.Category;
import rest.model.entity.Item;
import rest.service.ItemsService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

/**
 * Created by d1l on 5/19/17.
 */
@Service
public class ItemsServiceImpl implements ItemsService {

    private static final String SELECT_QUERY = "select i from Item i ";

    @Autowired
    RawQueryRepository<Item> rawQueryRepository;

    @Override
    @Transactional
    public List<Item> getItems(Map<String, String> parameters) {
        String query = this.buildQuery(parameters);
        return rawQueryRepository.executeQuery(query);
    }

    private String buildQuery(Map<String, String> parameters) {
        StringBuilder query = new StringBuilder();
        query.append(SELECT_QUERY);

        for (Map.Entry<String, String> param: parameters.entrySet()) {
            switch (param.getKey()) {
                case "category":
                    query.append("where i.category.name = '");
                    query.append(param.getValue());
                    query.append("'");
                    break;
                case "status":
                    query.append("where i.itemStatus.name = '");
                    query.append(param.getValue());
                    query.append("'");
                    break;
                case "minPrice":
                    query.append("where i.price >= ");
                    query.append(param.getValue());
                    query.append("");
                    break;
                case "maxPrice":
                    query.append("where i.price <= ");
                    query.append(param.getValue());
                    query.append("");
                    break;
                case "keywords":
                    query.append("where i.keywords in (");
                    String[] keywords = param.getValue().split(",");
                    for (String keyword : keywords) {
                        query.append("'");
                        query.append(keyword);
                        query.append("'");
                    }
                    query.append(")");
                    break;
                case "name":
                    query.append("where upper(i.name) like '");
                    query.append(param.getValue().toUpperCase());
                    query.append("%'");
                    break;
                case "description":
                    query.append("where upper(i.description) like '%");
                    query.append(param.getValue().toUpperCase());
                    query.append("%'");
                    break;
                case "sortingAsc":
                    query.append(" orderby ");
                    query.append(param.getValue());
                    query.append("");
                    break;
                case "sortingDesc":
                    query.append(" orderby ");
                    query.append(param.getValue());
                    query.append(" desc");
                    break;
            }
        }

        return query.toString();
    }

}
