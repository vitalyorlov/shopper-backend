package rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import rest.constants.Roles;
import rest.dao.MediaRepository;
import rest.dao.RoleRepository;
import rest.dao.UserRepository;
import rest.model.entity.Media;
import rest.model.entity.User;
import rest.service.UserService;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MediaRepository mediaRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public User save(User user) {
        Media media = new Media();
        media.setFilename("userlogo.jpg");
        media.setUrl("url");
        media = mediaRepository.save(media);
        user.setMedia(media);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRole(roleRepository.findByName(Roles.USER));
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }
}
