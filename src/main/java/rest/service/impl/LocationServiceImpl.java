package rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.dao.CityRepository;
import rest.dao.CountryRepository;
import rest.model.entity.City;
import rest.model.entity.Country;
import rest.service.LocationService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CityRepository cityRepository;

    @Override
    @Transactional
    public List<Country> getCountries() {
        return (List<Country>)countryRepository.findAll();
    }

    @Override
    @Transactional
    public List<City> getCitiesByCountryId(String countryId) {
        return cityRepository.findByCountryId(Integer.valueOf(countryId));
    }
}
