package rest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rest.dao.*;
import rest.model.entity.PersonalInfo;
import rest.model.entity.User;
import rest.service.PersonalInfoService;

import javax.transaction.Transactional;

@Service
public class PersonalInfoServiceImpl implements PersonalInfoService {

    @Autowired
    private PersonalInfoRepository personalInfoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CityRepository cityRepository;

    @Override
    @Transactional
    public PersonalInfo save(PersonalInfo personalInfo) {
        personalInfo = personalInfoRepository.save(personalInfo);
        return personalInfo;
    }

    @Override
    @Transactional
    public PersonalInfo findByUser(User user) {
        User dbUser = userRepository.findByLogin(user.getLogin());
        return personalInfoRepository.findByUserId(dbUser.getId());
    }
}
