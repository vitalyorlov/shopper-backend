package rest.service;

import rest.model.entity.City;
import rest.model.entity.Country;

import java.util.List;

public interface LocationService {

    List<Country> getCountries();

    List<City> getCitiesByCountryId(String countryId);

}
