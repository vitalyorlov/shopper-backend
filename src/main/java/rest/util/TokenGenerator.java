package rest.util;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.SecureRandom;

public class TokenGenerator {

    protected static SecureRandom random = new SecureRandom();

    public static synchronized String generateToken(String username, String password) {
        String credentials = username + password;

        String hashToken = "";
        try {
            hashToken = DatatypeConverter.printHexBinary(
                    MessageDigest.getInstance("MD5").digest(credentials.getBytes("UTF-8")));
        } catch (Exception ex) {}

        return hashToken;
    }

}